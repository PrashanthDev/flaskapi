from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Welcome to index page"

@app.route("/api/v1/")
def welcome():
    return "Welcome to first page!!"

@app.route("/api/v1/greet/<string:name>", methods=['GET'])
def api_salute(name: str):
    return f"Hai, {name}!"

if __name__ == "__main__":
    app.run()
